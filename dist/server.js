"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const port = 8888;
app.get("/", (req, res) => {
    res.send(`${"Hello! Mr.Santosh Swain."}`);
});
app.listen(port, () => {
    console.log(`Your node server is running on port: ${port}`);
});
//# sourceMappingURL=server.js.map