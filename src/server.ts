import express from 'express'
const app = express();

const port = 8888;


app.get("/", (req, res) => {
    res.send(`${"Hello! Mr.Santosh Swain."}`)
})

app.listen(port, () => {
    console.log(`Your node server is running on port: ${port}`)
})

